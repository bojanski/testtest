import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.util.concurrent.TimeUnit;


public class Home {
    public static WebDriver driver;


    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "/Users/boyanatanasov/Downloads/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/");
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);

    }


    @org.testng.annotations.Test
    public void open() {
        WebElement pageName = driver.findElement(By.xpath("//div[@id='site-name']//h1"));
        pageName.getText();
        Assert.assertEquals(pageName.getText(), "Selenium Easy", "That is not the name of the page!!! BUG!");

    }

    @org.testng.annotations.Test
    public void menu() throws InterruptedException {
        WebElement menuMenu =driver.findElement(By.xpath("//div[@id='navbar-collapse']//ul[@class='menu nav navbar-nav']//li[@class='expanded active dropdown']"));
        menuMenu.click();
        driver.findElement(By.xpath("//a[@href[1]='/selenium-tutorials']")).click();
        Thread.sleep(10000);



        Thread.sleep(10000);

    }


    @AfterMethod
    public void shutDown() {
        driver.quit();
    }
}
